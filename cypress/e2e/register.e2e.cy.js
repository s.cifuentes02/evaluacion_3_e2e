describe("Registration E2E", () => {
  it("Successful registration", () => {
    cy.visit("/register");
    cy.get('input[name="register-email"]').type("s.cifuentes02@ufromail.cl");
    cy.get('input[name="register-name"]').type("Saul");
    cy.get('input[name="register-rut"]').type("19.000.051-6");
    cy.get('input[name="register-password"]').first().type("Password123!");
    cy.get('input[name="register-password"]').eq(1).type("Password123!");
    cy.get('button[type="submit"]').click();

    cy.get("div")
      .find(".q-notification")
      .should("be.visible")
      .and("contain.text", "Please login");

    cy.visit("/login");
  });

  it("Failed registration due to email already in use", () => {
    cy.visit("/register");
    cy.get('input[name="register-email"]').type("s.cifuentes02@ufromail.cl"); // Email used in the R-1 test
    cy.get('input[name="register-name"]').type("Saul");
    cy.get('input[name="register-rut"]').type("19.000.051-6"); // Another valid RUT for the test
    cy.get('input[name="register-password"]').first().type("Password123!");
    cy.get('input[name="register-password"]').eq(1).type("Password123!");
    cy.get('button[type="submit"]').click();

    cy.get("div")
      .find(".q-notification")
      .should("be.visible")
      .and("contain.text", "the email already exists");
  });
  it("Failed registration due to duplicate RUT", () => {
    cy.visit("/register");
    cy.get('input[name="register-email"]').type("example1@example.com"); // Provide a valid email
    cy.get('input[name="register-name"]').type("John");
    cy.get('input[name="register-rut"]').type("19.000.051-6"); // RUT used in the R-1 test case
    cy.get('input[name="register-password"]').first().type("Password123!");
    cy.get('input[name="register-password"]').eq(1).type("Password123!");
    cy.get('button[type="submit"]').click();

    cy.get("div")
      .find(".q-notification")
      .should("be.visible")
      .and("contain.text", "the rut already exists");
  });
});