describe("Login E2E", () => {
	it("Login incorrect", () => {
		cy.visit("/login");
		cy.get('input[id="login-email"]').type("john@example.com");
		cy.get('input[id="login-password"]').type("john@example.com");
		cy.get('button[id="login-submit"]').click();

		cy.get("div")
			.find(".q-notification")
			.should("be.visible")
			.and("contain.text", "invalid credentials");
	});

	it("Correct login", () => {
		cy.visit("/login");
		cy.get('input[type="email"]').type("john@example.com");
		cy.get('input[type="password"]').type("2aSsword95%");
		cy.get('button[type="submit"]').click();

		cy.on("url:changed", () => {
			cy.url().should("eq", `${Cypress.config().baseUrl}/`);
		});
	});
	it("Login incorrect due to incorrect email", () => {
		cy.visit("/login");
		cy.get('input[type="email"]').type("emailincorrecto@gmil"); // Email sin el formato adecuado
		cy.get('input[type="password"]').type("contraseña123");
		cy.get('button[type="submit"]').click();

		cy.get("div")
			.find(".q-notification")
			.should("be.visible")
			.and("contain.text", "email is required and must be a valid email");

	});
	it("Correct login with unverified user", () => {
		cy.visit("/login");
		cy.get('input[type="email"]').type("s.cifuentes02@ufromail.cl"); // Utilizar el email de un usuario no verificado
		cy.get('input[type="password"]').type("Password123!");
		cy.get('button[type="submit"]').click();
	
		cy.get("div")
		  .find(".q-notification")
		  .should("be.visible")
		  .and("contain.text", "You need to verify your account");
	
		cy.url().should("include", "/verify");
		cy.url().should("include", "verify_error=1");
		
	  });
});
